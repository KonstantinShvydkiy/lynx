<?php

namespace App\Repository;

use App\Entity\Article;
use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    // /**
    //  * @return Article[] Returns an array of Article objects
    //  */
    public function findByTextAndCategory(int $limit = 10, int $offset = 0, ?string $searchString = null, ?array $categoryIds = null)
    {
        $queryBuilder = $this->createQueryBuilder("a")
            ->addSelect("c")
            ->leftJoin("a.categories", "c")
            ->where('a.deleted_at IS NULL')
            ->andWhere('c.deleted_at IS NULL')
            ->orderBy('a.id', 'ASC')
            ->setFirstResult($offset)
            ->setMaxResults($limit);

        if ($categoryIds && count($categoryIds)) {
            $queryBuilder->andWhere($queryBuilder->expr()->in('c.id', $categoryIds));
        }

        if ($searchString) {
            $queryBuilder
                ->andWhere('MATCH_AGAINST(a.title, a.text) AGAINST(:searchString) > 0')
                ->setParameter('searchString', $searchString);
        }

        return new Paginator($queryBuilder->getQuery());

    }

    public function find($id, $lockMode = null, $lockVersion = null): ?Article
    {
        return $this->createQueryBuilder("a")
            ->addSelect("c")
            ->leftJoin("a.categories", "c")
            ->where("a.id = :id")
            ->andWhere('c.deleted_at IS NULL')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

}
