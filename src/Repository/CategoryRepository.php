<?php

namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
    }

    /**
     * @return Category[] Returns an array of Category objects
     */
    public function findActiveByIds(array $ids)
    {
        $qb = $this->createQueryBuilder('c')
            ->where('c.deleted_at IS NULL')
            ->orderBy('c.id', 'ASC');

        $qb->andWhere($qb->expr()->in('c.id', $ids));

        return $qb->getQuery()->getResult();
    }


    public function findOneActive(int $id): ?Category
    {
        return $this->createQueryBuilder('c')
            ->Where('c.deleted_at IS NULL')
            ->andWhere('c.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
