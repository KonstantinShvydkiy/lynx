<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Article;
use App\Entity\Pagiator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;


/**
 * @Route("/lynx/articles", name="lynx_", format="json")
 */
//todo должны ли быть null в ответе
class ArticleController extends AbstractController
{

    /**
     * @Route("/{id}", requirements={"id"="\d+"}, methods={"GET"}, name="article")
     * @param Article|null $article
     * @return JsonResponse
     * @throws NotFoundHttpException
     */
    public function getArticle(?Article $article = null): JsonResponse
    {
        if ($article === null) {
            throw new NotFoundHttpException("Article not found");
        }

        return $this->json($article);
    }

    /**
     * @Route("/{id}/categories", requirements={"id"="\d+"}, methods={"GET"})
     * @param Article|null $article
     * @return JsonResponse
     * @throws NotFoundHttpException
     */
    public function getArticleCategories(?Article $article = null): JsonResponse
    {
        if ($article === null) {
            throw new NotFoundHttpException("Article not found");
        }

        return $this->json($article->getCategories());
    }

    /**
     * @Route("/{id}/categories", requirements={"id"="\d+"}, methods={"PUT"})
     * @param Request $request
     * @param Article|null $article
     * @return JsonResponse
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function setArticleCategories(Request $request, ?Article $article = null): JsonResponse
    {
        if ($article === null) {
            throw new NotFoundHttpException("Article not found");
        }

        $categoryIds = $request->query->get('categories');

        if (!is_array($categoryIds)) {
            throw new BadRequestHttpException("Categories must bee array");
        }

        $res = $this->initAndCheckCategoriesByIdsArray($categoryIds);

        $categories = $res->categories;
        $notfoundIds = $res->notFoundIds;

        if (count($notfoundIds)) {
            throw new NotFoundHttpException("Can't find categories with ids: " . join(',', $notfoundIds));
        }

        $article->setCaterories($categories);

        $this->getDoctrine()->getManager()->flush();

        return $this->json($article->getCategories());
    }

    /**
     * @Route("/{id}/categories", requirements={"id"="\d+"}, methods={"POST"})
     * @param Request $request
     * @param Article|null $article
     * @return JsonResponse
     * @throws NotFoundHttpException
     */
    public function addArticleCategory(Request $request, ?Article $article = null): JsonResponse
    {
        if ($article === null) {
            throw new NotFoundHttpException("Article not found");
        }

        $categoryId = $request->query->getInt('category');

        $category = $this->getDoctrine()
            ->getRepository(Category::class)
            ->findOneActive($categoryId);

        if ($category === null) {
            throw new NotFoundHttpException("No active category found for id = $categoryId");
        }

        $article->addCategory($category);

        $this->getDoctrine()->getManager()->flush();

        return $this->json($article->getCategories());
    }


    /**
     * @Route("/{article_id}/categories/{caterory_id}", requirements={"article_id"="\d+", "caterory_id"="\d+"}, methods={"DELETE"})
     * @Entity("article", expr="repository.find(article_id)")
     * @Entity("category", expr="repository.find(caterory_id)")
     * @throws NotFoundHttpException
     */
    public function deleteArticleCategory(?Article $article = null, ?Category $category = null): JsonResponse
    {
        if ($article === null) {
            throw new NotFoundHttpException('Article not found');
        }

        if ($category === null) {
            throw new NotFoundHttpException('Category not found');
        }

        if (!$article->getCategories()->contains($category)) {
            throw new NotFoundHttpException("Article {$article->getId()} has not category {$category->getId()}");
        }

        $article->removeCategory($category);

        $this->getDoctrine()->getManager()->flush();
        //todo подумать над ответом
        return $this->json($article->getCategories(), Response::HTTP_NO_CONTENT);
    }


    /**
     * @Route("", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function getActiveArticles(Request $request): JsonResponse
    {
        $limit = $request->query->getInt('limit', 10);
        $offset = $request->query->getInt('offset', 0);
        $categoryIds = $request->query->get('categories');
        $searchString = $request->query->get('search_string');


        if ($limit > 10) {
            throw new BadRequestHttpException("Limit too big");
        }

        if ($offset < 0) {
            throw new BadRequestHttpException("Offset must be positive");
        }

        if (($searchString !== null) && (mb_strlen($searchString) < 3)) {
            throw new BadRequestHttpException("SearchString must be 3 symbols at least");
        }

        if ($categoryIds !== null) {

            if (!is_array($categoryIds)) {
                throw new BadRequestHttpException("Categories must bee array or miss");
            }

            $res = $this->initAndCheckCategoriesByIdsArray($categoryIds);

            $categoryIds = $res->foundIds;
            $notfoundIds = $res->notFoundIds;

            if (count($notfoundIds)) {
                throw new NotFoundHttpException("Can't find categories with ids: " . join(',', $notfoundIds));
            }
        }

        $articles = $this->getDoctrine()
            ->getRepository(Article::class)
            ->findByTextAndCategory(
                $limit,
                $offset,
                $searchString,
                $categoryIds
            );

        $res = new Pagiator();
        $res->setElements($articles);
        $res->setTotalElementsNumber(count($articles));

        return $this->json($res);
    }


    /**
     * @Route("",  methods={"POST"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return JsonResponse
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function createArticle(Request $request, ValidatorInterface $validator): JsonResponse
    {
        $title = $request->query->get('title');
        $text = $request->query->get('text');
        $categoryIds = $request->query->get('categories');


        if ($title === null) {
            throw new NotFoundHttpException('name must be not empty');
        }

        $article = new Article();

        $article->setTitle($title);

        $errors = $validator->validate($article);
        if (count($errors)) {
            throw new BadRequestHttpException("Icorrect article title");
        }

        $article->setText($text);

        if ($categoryIds !== null) {

            if (!is_array($categoryIds)) {
                throw new BadRequestHttpException("Categories must bee array");
            }

            $res = $this->initAndCheckCategoriesByIdsArray($categoryIds);

            $categories = $res->categories;
            $notfoundIds = $res->notFoundIds;

            if (count($notfoundIds)) {
                throw new NotFoundHttpException("Can't find categories with ids: " . join(',', $notfoundIds));
            }

            $article->setCaterories($categories);
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($article);
        $entityManager->flush();

        return $this->json(
            $article,
            Response::HTTP_CREATED,
            array('Location' => $this->generateUrl(
                'lynx_article',
                array('id' => $article->getId()))
            )
        );
    }

    /**
     * @Route("/{id}",  methods={"DELETE"})
     * @param Article|null $article
     * @return JsonResponse
     * @throws NotFoundHttpException
     */
    public function deleteArticle(?Article  $article = null): JsonResponse
    {
        if ($article === null) {
            throw new NotFoundHttpException("Article not found");
        }

        $article->delete();

        $this->getDoctrine()->getManager()->flush();

        return $this->json($article, Response::HTTP_NO_CONTENT);
    }


    /**
     * @Route("/{id}",  methods={"PATCH"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param Article|null $article
     * @return JsonResponse
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function updateArticle(Request $request, ValidatorInterface $validator, ?Article  $article = null): JsonResponse
    {
        if ($article === null) {
            throw new NotFoundHttpException("Article not found");
        }

        $title = $request->query->get('title');
        $text = $request->query->get('text');
        $categoryIds = $request->query->get('categories');

        // update only passed property
        if ($title !== null) {
            $article->setTitle($title);

            $errors = $validator->validate($article);
            if (count($errors)) {
                throw new BadRequestHttpException("Incorrect article title");
            }
        }

        if ($text !== null) {
            $article->setText($text);
        }

        if ($categoryIds !== null) {

            if (!is_array($categoryIds)) {
                throw new BadRequestHttpException("Categories must bee array");
            }

            $res = $this->initAndCheckCategoriesByIdsArray($categoryIds);

            $categories = $res->categories;
            $notfoundIds = $res->notFoundIds;

            if (count($notfoundIds)) {
                throw new NotFoundHttpException("Can't find categories with ids: " . join(',', $notfoundIds));
            }

            $article->setCaterories($categories);
        }


        $this->getDoctrine()->getManager()->flush();

        return $this->json($article);
    }


    private function initAndCheckCategoriesByIdsArray(array $categoryIds)
    {

        $categories = $this->getDoctrine()
            ->getRepository(Category::class)
            ->findActiveByIds($categoryIds);

        $foundIds = array();

        foreach ($categories as $category) {
            $foundIds[] = $category->getId();
        }

        $notfoundIds = array_diff($categoryIds, $foundIds);

        $res = new \stdClass();
        $res->categories = $categories;
        $res->foundIds = $foundIds;
        $res->notFoundIds = $notfoundIds;

        return $res;
    }
}