<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;


/**
 * @Route("/lynx/categories", name="lynx_", format="json")
 */
//todo должны ли быть null в ответе
class CategoryController extends AbstractController
{

    /**
     * @Route("", methods={"GET"})
     * @return JsonResponse
     */
    public function getActiveCategories(): JsonResponse
    {
        $categories = $this->getDoctrine()
            ->getRepository(Category::class)
            ->findBy(
                ['deleted_at' => null]
            );
        return $this->json($categories);
    }


    /**
     * @Route("/{id}", requirements={"id"="\d+"}, methods={"GET"}, name="category")
     * @param Category|null $category
     * @return JsonResponse
     * @throws NotFoundHttpException
     */
    public function getCategory(?Category $category = null): JsonResponse
    {
        if ($category === null) {
            throw new NotFoundHttpException("Category not found");
        }
        //todo правильный эксепшен
        return $this->json($category);
    }


    /**
     * @Route("",  methods={"POST"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return JsonResponse
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function createCategory(Request $request, ValidatorInterface $validator): JsonResponse
    {
        $name = $request->query->get('name');

        if ($name === null) {
            throw new NotFoundHttpException('Please specify new category name');
        }

        $category = new Category();
        $category->setName($name);

        $errors = $validator->validate($category);
        if (count($errors)) {
            throw new BadRequestHttpException("Icorrect category name");
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($category);
        $entityManager->flush();

        return $this->json(
            $category,
            Response::HTTP_CREATED,
            array('Location' => $this->generateUrl(
                'lynx_category',
                array('id' => $category->getId()))
            )
        );

    }


    /**
     * @Route("/{id}", requirements={"id"="\d+"}, methods={"DELETE"})
     * @param Category|null $category
     * @return JsonResponse
     * @throws NotFoundHttpException
     */
    public function deleteCategory(?Category $category = null): JsonResponse
    {
        if ($category === null) {
            throw new NotFoundHttpException("Category not found");
        }

        $category->delete();

        $this->getDoctrine()->getManager()->flush();

        return $this->json($category, Response::HTTP_NO_CONTENT);
        //todo удалить связи
    }


    /**
     * @Route("/{id}", requirements={"id"="\d+"},  methods={"PATCH"})
     * @param Request $request
     * @param Category|null $category
     * @param ValidatorInterface $validator
     * @return JsonResponse
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function updateCategory(Request $request, ValidatorInterface $validator, ?Category $category = null): JsonResponse
    {
        $name = $request->query->get('name');

        if ($name === null) {
            throw new NotFoundHttpException('Please specify new category name');
        }

        $category->setName($name);

        $errors = $validator->validate($category);
        if (count($errors)) {
            throw new BadRequestHttpException("Icorrect category name");
        }

        $this->getDoctrine()->getManager()->flush();

        return $this->json($category);
    }

}