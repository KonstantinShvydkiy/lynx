<?php

namespace App\Entity;
use Doctrine\Common\Collections\Collection;

class Pagiator
{
    /** @var int */
    private $total_elements_number = 0;

    /** @var array|Collection */
    private $elements = array();

    public function getTotalElementsNumber(): int
    {
        return $this->total_elements_number;
    }

    public function setTotalElementsNumber(int $number): self
    {
        $this->total_elements_number = $number;
        return $this;
    }

    public function getElements()
    {
        return $this->elements;
    }

    public function setElements($elements): self
    {
        $this->elements = $elements;
        return $this;
    }
}
