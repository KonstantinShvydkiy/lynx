<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArticleRepository")
 * @ORM\Table(name="tbl_article",
 *     indexes={@ORM\Index(columns={"title", "text"}, flags={"fulltext"}),
 *              @ORM\Index(columns={"deleted_at"})})
 */
class Article
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *      min = 3,
     *      max = 255
     * )
     * @Assert\NotBlank
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $text;

    /**
     * @ORM\Column(type="datetime", nullable=true, columnDefinition="TIMESTAMP NULL DEFAULT NULL")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true, columnDefinition="TIMESTAMP NULL DEFAULT NULL")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true, columnDefinition="TIMESTAMP NULL DEFAULT NULL")
     */
    private $deleted_at;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Category")
     */
    private $categories;

    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->categories = new ArrayCollection();
    }
    

    public function getId(): ?int
    {
        return $this->id;
    }

    private function setUpdateTime()
    {
        if ($this->id)
            $this->updated_at = new \DateTime();
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        if ($this->title !== $title) {
            $this->title = $title;
            $this->setUpdateTime();
        }
        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(?string $text): self
    {
        if ($this->text !== $text) {
            $this->text = $text;
            $this->setUpdateTime();
        }
        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deleted_at;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
            $this->setUpdateTime();
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->categories->contains($category)) {
            $this->categories->removeElement($category);
            $this->setUpdateTime();
        }

        return $this;
    }

    public function setCaterories(array $categories): self
    {
        foreach ($this->categories as $category) {
            if (!in_array($category, $categories)) {
                $this->removeCategory($category);
            }
        }

        foreach ($categories as $category) {
            $this->addCategory($category);
        }

        return $this;
    }

    public function delete()
    {
        $this->deleted_at = new \DateTime();
    }

}
